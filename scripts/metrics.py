# importing moduli
from .segregation import segregate_hosts, segregate_renters
import numpy as np
import math

# cut data frame with respect to age thresholds
def cut_thresholds(data_frame):
    
    # define age thresholds
    index_array = []
    for i in range(0, len(data_frame)):
        
        # thresholds for renter age
        if math.isnan(data_frame['renter age min'].iloc[i]):
            if not math.isnan(data_frame['renter age max'].iloc[i]):
                if data_frame['renter age max'].iloc[i] < 18 or data_frame['renter age max'].iloc[i] > 35:
                    index_array.append(i)
        elif math.isnan(data_frame['renter age max'].iloc[i]):
            if not math.isnan(data_frame['renter age min'].iloc[i]):
                if data_frame['renter age min'].iloc[i] < 18 or data_frame['renter age min'].iloc[i] > 35:
                    index_array.append(i)
        elif not math.isnan(data_frame['renter age min'].iloc[i]) and not math.isnan(data_frame['renter age max'].iloc[i]):
            if data_frame['renter age min'].iloc[i] < 18 or data_frame['renter age min'].iloc[i] > 35:
                    index_array.append(i)
            elif data_frame['renter age max'].iloc[i] < 18 or data_frame['renter age max'].iloc[i] > 35:
                    index_array.append(i)

        # threshold for hosts age
        if math.isnan(data_frame['hosts age min'].iloc[i]):
            if not math.isnan(data_frame['hosts age max'].iloc[i]):
                if data_frame['hosts age max'].iloc[i] < 18 or data_frame['hosts age max'].iloc[i] > 35:
                    index_array.append(i)
        elif math.isnan(data_frame['hosts age max'].iloc[i]):
            if not math.isnan(data_frame['hosts age min'].iloc[i]):
                if data_frame['hosts age min'].iloc[i] < 18 or data_frame['hosts age min'].iloc[i] > 35:
                    index_array.append(i)
        elif not math.isnan(data_frame['hosts age min'].iloc[i]) and not math.isnan(data_frame['hosts age max'].iloc[i]):
            if data_frame['hosts age min'].iloc[i] < 18 or data_frame['hosts age min'].iloc[i] > 35:
                    index_array.append(i)
            elif data_frame['hosts age max'].iloc[i] < 18 or data_frame['hosts age max'].iloc[i] > 35:
                    index_array.append(i)
                    
    # droping indexes
    index_final = np.array(index_array).T.tolist()
    data_frame = data_frame.drop(index_final)
    
    return data_frame

# calculate correspondence of age group of hosts with social status preferences
def metrics_social(data_frame):
        
    # cutting the thresholds
    data_frame = cut_thresholds(data_frame)
    
    # performing segregation
    data_frame = segregate_renters(data_frame)
            
    # calculating metrics
    social_info = []
    
    # group 18-20
    const = len(data_frame[data_frame["renter age 18-19"] == "1"])
    social_info.append(len(data_frame[(data_frame["renter age 18-19"] == "1") & (data_frame["Studenten-WG"] == 1)])/const)
    social_info.append(len(data_frame[(data_frame["renter age 18-19"] == "1") & (data_frame["Berufstätigen-WG"] == 1)])/const)
    social_info.append(len(data_frame[(data_frame["renter age 18-19"] == "1") & (data_frame["LGBTQIA+"] == 1)])/const)

    # group 21-23
    const = len(data_frame[data_frame["renter age 20-25"] == "1"])
    social_info.append(len(data_frame[(data_frame["renter age 20-25"] == "1") & (data_frame["Studenten-WG"] == 1)])/const)
    social_info.append(len(data_frame[(data_frame["renter age 20-25"] == "1") & (data_frame["Berufstätigen-WG"] == 1)])/const)
    social_info.append(len(data_frame[(data_frame["renter age 20-25"] == "1") & (data_frame["LGBTQIA+"] == 1)])/const)

    # group 24-28
    const = len(data_frame[data_frame["renter age 26-30"] == "1"])
    social_info.append(len(data_frame[(data_frame["renter age 26-30"] == "1") & (data_frame["Studenten-WG"] == 1)])/const)
    social_info.append(len(data_frame[(data_frame["renter age 26-30"] == "1") & (data_frame["Berufstätigen-WG"] == 1)])/const)
    social_info.append(len(data_frame[(data_frame["renter age 26-30"] == "1") & (data_frame["LGBTQIA+"] == 1)])/const)

    # group 29-35
    const = len(data_frame[data_frame["renter age 31-35"] == "1"])
    social_info.append(len(data_frame[(data_frame["renter age 31-35"] == "1") & (data_frame["Studenten-WG"] == 1)])/const)
    social_info.append(len(data_frame[(data_frame["renter age 31-35"] == "1") & (data_frame["Berufstätigen-WG"] == 1)])/const)
    social_info.append(len(data_frame[(data_frame["renter age 31-35"] == "1") & (data_frame["LGBTQIA+"] == 1)])/const)
    
    # any age
    const = len(data_frame)
    social_info.append(len(data_frame[data_frame["Studenten-WG"] == 1])/const)
    social_info.append(len(data_frame[data_frame["Berufstätigen-WG"] == 1])/const)
    
    return social_info
    
# calculate correspondence of age group of hosts with Zweck preferences
def metrics_zweck(data_frame):
        
    # cutting the thresholds
    data_frame = cut_thresholds(data_frame)
    
    # performing segregation
    data_frame = segregate_hosts(data_frame)
        
    # calculating metrics
    zweck_info = []
    
    # group 18-20
    const = len(data_frame[data_frame["hosts age 18-20"] == "1"])
    zweck_info.append(len(data_frame[(data_frame["hosts age 18-20"] == "1") & (data_frame["Zweck-WG"] == "1")])/const)
    zweck_info.append(len(data_frame[(data_frame["hosts age 18-20"] == "1") & (data_frame["Zweck-WG"] == "0")])/const)
    zweck_info.append(len(data_frame[(data_frame["hosts age 18-20"] == "1") & (data_frame["Zweck-WG"] == "Not defined")])/const)

    # group 21-23
    const = len(data_frame[data_frame["hosts age 21-23"] == "1"])
    zweck_info.append(len(data_frame[(data_frame["hosts age 21-23"] == "1") & (data_frame["Zweck-WG"] == "1")])/const)
    zweck_info.append(len(data_frame[(data_frame["hosts age 21-23"] == "1") & (data_frame["Zweck-WG"] == "0")])/const)
    zweck_info.append(len(data_frame[(data_frame["hosts age 21-23"] == "1") & (data_frame["Zweck-WG"] == "Not defined")])/const)

    # group 24-28
    const = len(data_frame[data_frame["hosts age 24-28"] == "1"])
    zweck_info.append(len(data_frame[(data_frame["hosts age 24-28"] == "1") & (data_frame["Zweck-WG"] == "1")])/const)
    zweck_info.append(len(data_frame[(data_frame["hosts age 24-28"] == "1") & (data_frame["Zweck-WG"] == "0")])/const)
    zweck_info.append(len(data_frame[(data_frame["hosts age 24-28"] == "1") & (data_frame["Zweck-WG"] == "Not defined")])/const)

    # group 29-35
    const = len(data_frame[data_frame["hosts age 29-35"] == "1"])
    zweck_info.append(len(data_frame[(data_frame["hosts age 29-35"] == "1") & (data_frame["Zweck-WG"] == "1")])/const)
    zweck_info.append(len(data_frame[(data_frame["hosts age 29-35"] == "1") & (data_frame["Zweck-WG"] == "0")])/const)
    zweck_info.append(len(data_frame[(data_frame["hosts age 29-35"] == "1") & (data_frame["Zweck-WG"] == "Not defined")])/const)
    
    return zweck_info

# calculate correspondence of age group of renters and hosts
def metrics_age(data_frame):
    
    # cutting the thresholds
    data_frame = cut_thresholds(data_frame)
        
    # performing segregation
    data_frame = segregate_hosts(segregate_renters(data_frame))
        
    # calculating metrics
    age_info = []
    
    # group 18-20
    const = len(data_frame[data_frame["hosts age 18-20"] == "1"])
    age_info.append(len(data_frame[(data_frame["hosts age 18-20"] == "1") & (data_frame["renter age 18-19"] == "1")])/const)
    age_info.append(len(data_frame[(data_frame["hosts age 18-20"] == "1") & (data_frame["renter age 20-25"] == "1")])/const)
    age_info.append(len(data_frame[(data_frame["hosts age 18-20"] == "1") & (data_frame["renter age 26-30"] == "1")])/const)
    age_info.append(len(data_frame[(data_frame["hosts age 18-20"] == "1") & (data_frame["renter age 31-35"] == "1")])/const)

    # group 21-23
    const = len(data_frame[data_frame["hosts age 21-23"] == "1"])
    age_info.append(len(data_frame[(data_frame["hosts age 21-23"] == "1") & (data_frame["renter age 18-19"] == "1")])/const)
    age_info.append(len(data_frame[(data_frame["hosts age 21-23"] == "1") & (data_frame["renter age 20-25"] == "1")])/const)
    age_info.append(len(data_frame[(data_frame["hosts age 21-23"] == "1") & (data_frame["renter age 26-30"] == "1")])/const)
    age_info.append(len(data_frame[(data_frame["hosts age 21-23"] == "1") & (data_frame["renter age 31-35"] == "1")])/const)

    # group 24-28
    const = len(data_frame[data_frame["hosts age 24-28"] == "1"])
    age_info.append(len(data_frame[(data_frame["hosts age 24-28"] == "1") & (data_frame["renter age 18-19"] == "1")])/const)
    age_info.append(len(data_frame[(data_frame["hosts age 24-28"] == "1") & (data_frame["renter age 20-25"] == "1")])/const)
    age_info.append(len(data_frame[(data_frame["hosts age 24-28"] == "1") & (data_frame["renter age 26-30"] == "1")])/const)
    age_info.append(len(data_frame[(data_frame["hosts age 24-28"] == "1") & (data_frame["renter age 31-35"] == "1")])/const)

    # group 29-35
    const = len(data_frame[data_frame["hosts age 29-35"] == "1"])
    age_info.append(len(data_frame[(data_frame["hosts age 29-35"] == "1") & (data_frame["renter age 18-19"] == "1")])/const)
    age_info.append(len(data_frame[(data_frame["hosts age 29-35"] == "1") & (data_frame["renter age 20-25"] == "1")])/const)
    age_info.append(len(data_frame[(data_frame["hosts age 29-35"] == "1") & (data_frame["renter age 26-30"] == "1")])/const)
    age_info.append(len(data_frame[(data_frame["hosts age 29-35"] == "1") & (data_frame["renter age 31-35"] == "1")])/const)
    
    return age_info
    
# calculate share for each integer age
def metrics_age_renters(data_frame):
    
    # cutting the thresholds
    data_frame = cut_thresholds(data_frame)

    # calculate share
    count_age = []
    a = int(data_frame["renter age min"].min())
    b = int(data_frame["renter age max"].max()) + 1
    
    for x in range(a, b):
        count_current = 0
        for ad in range(0, len(data_frame)):
            if math.isnan(data_frame["renter age min"].iloc[ad]): #  == ' '
                if math.isnan(data_frame["renter age max"].iloc[ad]):
                    count_current = count_current + 1
                else:
                    if x <= int(data_frame["renter age max"].iloc[ad]):
                        count_current = count_current + 1
            else:
                if x >= int(data_frame["renter age min"].iloc[ad]):
                    if math.isnan(data_frame["renter age max"].iloc[ad]):
                        count_current = count_current + 1
                    else:
                        if x <= int(data_frame["renter age max"].iloc[ad]):
                            count_current = count_current + 1
        count_age.append(count_current)
    
    count_age = [count_age[i]/len(data_frame) for i in range(0, len(count_age))]
    
    return count_age, a, b

# calculate share for each integer age
def metrics_age_hosts(data_frame):
    
    # cutting the thresholds
    data_frame = cut_thresholds(data_frame)
    
    # calculate share
    count_age = []
    a = int(data_frame["hosts age min"].min())
    b = int(data_frame["hosts age max"].max()) + 1
    
    for x in range(a, b):
        count_current = 0
        for ad in range(0, len(data_frame)):
            if math.isnan(data_frame["hosts age min"].iloc[ad]): #  == ' '
                if math.isnan(data_frame["hosts age max"].iloc[ad]):
                    count_current = count_current + 1
                else:
                    if x <= int(data_frame["hosts age max"].iloc[ad]):
                        count_current = count_current + 1
            else:
                if x >= int(data_frame["hosts age min"].iloc[ad]):
                    if math.isnan(data_frame["hosts age max"].iloc[ad]):
                        count_current = count_current + 1
                    else:
                        if x <= int(data_frame["hosts age max"].iloc[ad]):
                            count_current = count_current + 1
        count_age.append(count_current)
    
    count_age = [count_age[i]/len(data_frame) for i in range(0, len(count_age))]
    
    return count_age, a, b

# calculate gender preferences for each group
def metrics_gender(data_frame):
    
    # initiating counters for households
    male_only = 0
    female_only = 0
    male_major = 0
    female_major = 0
    mixed_major = 0

    # initiating counters for male only households
    male_only_to_male = 0
    male_only_to_female = 0
    male_only_to_mixed = 0

    # initiating counters for female only households
    female_only_to_male = 0
    female_only_to_female = 0
    female_only_to_mixed = 0

    # initiating counters for male dominant households
    male_major_to_male = 0
    male_major_to_female = 0
    male_major_to_mixed = 0

    # initiating counters for female dominant households
    female_major_to_male = 0
    female_major_to_female = 0
    female_major_to_mixed = 0
    
    # initiating counters for mixed households
    mixed_to_male = 0
    mixed_to_female = 0
    mixed_to_mixed = 0
    
    # processing each add separately
    for male_hosts, female_hosts, renter_gender in zip(data_frame["male hosts"], data_frame["female hosts"], data_frame["renter gender"]):
    
        # processing male only households
        if male_hosts > 0 and female_hosts == 0:
            
            male_only = male_only + 1
            if renter_gender == "Male":
                male_only_to_male = male_only_to_male + 1
            if renter_gender == "Female":
                male_only_to_female = male_only_to_female + 1
            if renter_gender == "Not important":
                male_only_to_mixed = male_only_to_mixed + 1
         
        # processing female only households
        elif male_hosts == 0 and female_hosts > 0:
            
            female_only = female_only + 1
            if renter_gender == "Male":
                female_only_to_male = female_only_to_male + 1
            if renter_gender == "Female":
                female_only_to_female = female_only_to_female + 1
            if renter_gender == "Not important":
                female_only_to_mixed = female_only_to_mixed + 1
        
        # processing mixed households
        elif male_hosts > 0 and female_hosts > 0:
            
            # processing male dominant households, i.e., >50%
            if male_hosts > female_hosts:
                
                male_major = male_major + 1
                if renter_gender == "Male":
                    male_major_to_male = male_major_to_male + 1
                if renter_gender == "Female":
                    male_major_to_female = male_major_to_female + 1
                if renter_gender  == "Not important":
                    male_major_to_mixed = male_major_to_mixed + 1

            # processing female dominant households, i.e., >50%
            elif female_hosts > male_hosts:
                
                female_major = female_major + 1
                if renter_gender == "Male":
                    female_major_to_male = female_major_to_male + 1
                if renter_gender == "Female":
                    female_major_to_female = female_major_to_female + 1
                if renter_gender  == "Not important":
                    female_major_to_mixed = female_major_to_mixed + 1
            
            # processing equally dominant households
            else:

                mixed_major = mixed_major + 1
                if renter_gender == "Male":
                    mixed_to_male = mixed_to_male + 1
                if renter_gender == "Female":
                    mixed_to_female = mixed_to_female + 1
                if renter_gender  == "Not important":
                    mixed_to_mixed = mixed_to_mixed + 1
               
    # initiating metrics for male only households
    male_only_to_male = male_only_to_male/male_only
    male_only_to_female = male_only_to_female/male_only
    male_only_to_mixed = male_only_to_mixed/male_only

    # initiating metrics for female only households
    female_only_to_male = female_only_to_male/female_only
    female_only_to_female = female_only_to_female/female_only
    female_only_to_mixed = female_only_to_mixed/female_only

    # initiating metrics for male dominant households
    male_major_to_male = male_major_to_male/male_major
    male_major_to_female = male_major_to_female/male_major
    male_major_to_mixed = male_major_to_mixed/male_major

    # initiating metrics for female dominant households
    female_major_to_male = female_major_to_male/female_major
    female_major_to_female = female_major_to_female/female_major
    female_major_to_mixed = female_major_to_mixed/female_major

    # initiating metrics for mixed households
    mixed_to_male = mixed_to_male/mixed_major
    mixed_to_female = mixed_to_female/mixed_major
    mixed_to_mixed = mixed_to_mixed/mixed_major
    
    return (male_only_to_male, male_only_to_female, male_only_to_mixed, # male only
            female_only_to_male, female_only_to_female, female_only_to_mixed, # female only
            male_major_to_male, male_major_to_female, male_major_to_mixed, # male major
            female_major_to_male, female_major_to_female, female_major_to_mixed, # female major
            mixed_to_male, mixed_to_female, mixed_to_mixed) # mixed