from bs4 import BeautifulSoup
from datetime import date
import pandas as pd
import requests
import random
import math
import time
import re

# to scrape several entities for the given city
def scrape(URL, n_rows, time_min, time_plus):
    
    skipped = 0
    time_start = time.time()
    file_name = re.search(r'-in-([A-Za-z]+).', URL).group(1).lower() + '_angebote_' + date.today().strftime("%d-%m-%Y") + "_" + time.strftime('%X') + '.csv'

#     ads_info = []
    links = scrape_links(URL, n_rows, time_min, time_plus)
        
    for i in range(0, len(links.URL)):
        
        if i == 0:
            print(links.URL[i] + " : 0.0 sec")
        else:
            random_time = random.uniform(time_min, time_min + time_plus)
            print(links.URL[i] + " : " + str(random_time) + " sec")
            time.sleep(random_time) 
        
        ads_current = scrape_ad(links.URL[i])

        if ads_current != "":
    #         ads_info.append(ads_current)
            result = pd.DataFrame([ads_current])    
            result.to_csv(file_name, index = False, mode = 'w' if i == 0 else 'a', header = True if i == 0 else False)
        else:
            skipped = skipped + 1

#     result = pd.DataFrame(ads_info)
#     result.to_csv(file_name, index = False, header = True)
    
    time_end = time.time() - time_start
    
    return "Successfully scraped " + str(len(links.URL) - skipped) + " entities for " + str(time_end) + " sec.\nSee " + file_name

# to scrape several entities for the city
def scrape_links(URL_start, n_total, time_min, time_plus):
    
    links_info_raw = []
    
    for i in range(0, math.ceil(n_total/20)):
        
        curr_URL = URL_start.split('.html')[0][:-1] + str(i) + ".html"
        if i < math.ceil(n_total/20) - 1:
            curr_page = scrape_page(curr_URL, 20)
        else:
            curr_page = scrape_page(curr_URL, n_total%20)
        
        links_info_raw = links_info_raw + curr_page
        
        random_time = random.uniform(time_min, time_min + time_plus)
        print("Page " + str(i + 1) + ": " + str(random_time) + " sec")
        time.sleep(random_time)
    
    links_info = {"URL": links_info_raw}
    links = pd.DataFrame(links_info)
    links = links.drop_duplicates(subset = ['URL'])
    links = links.reset_index(drop = True)
    
    return links

# to scrape links from one page
def scrape_page(URL, n):

    r = requests.get(URL)
    page = BeautifulSoup(r.content, 'html5lib')
    links = []
    
    for row in page.findAll('div', attrs = {'class': 'wgg_card offer_list_item'}):
        links.append("https://www.wg-gesucht.de" + row.find('a')['href'])
        
    return links[0:n]

# to scrape one page that is an ad itself
def scrape_ad(URL):
    
    tag = "normal"
    r = requests.get(URL)
    soup = BeautifulSoup(r.content, 'html5lib')
    
    try:
        deactivated = soup.findAll('h4', attrs = {'class': 'headline alert-primary-headline'})
        deactivated = re.sub('\s+', ' ', deactivated[1].text.replace('\r', '').replace('\n', '').strip())
        if re.search(r'deaktiviert', deactivated):
            tag = "deactivated"
            print("Warning: ad deactivated, skipping!")
    except IndexError:
        tag = "normal"
        
    
    try:
        deleted = soup.findAll('div', attrs = {'class': 'alert-body'})
        deleted = re.sub('\s+', ' ', deleted[2].text.replace('\r', '').replace('\n', '').strip())
        if re.search(r'existiert nicht', deleted):
            tag = "deleted"
            print("Warning: ad deleted, skipping!")
    except IndexError:
        tag = "normal"
        
    if tag == "normal":
        ad_info = {}
        ad_info['URL'] = URL

        # rent
        rent = soup.findAll('h2', attrs = {'class': 'headline headline-key-facts'})
        if rent == None:
            ad_info['rent'] = " "
        else:
            ad_info['rent'] = rent[1].text.strip()

        # deposit
        deposit = soup.find('input', attrs = {'id': 'kaution'})
        if deposit == None:
            ad_info['deposit'] = " "
        else:
            ad_info['deposit'] = deposit['value'].strip() + "€"

        # address
        address = soup.find('a', attrs = {'style': 'line-height: 1.5em;font-weight: normal; color: #555; margin-bottom: 23px;'})
        if address == None:
            ad_info['address'] = " "
        else:
            ad_info['address'] = re.sub('\s+', ' ', address.text.replace('\r', '').replace('\n', '').strip())

        # date
        date = soup.find('p', attrs = {'style': 'line-height: 2em;'})
        if date == None:
            ad_info['date'] = " "
        else:
            ad_info['date'] = date.b.text.strip()

        # details
        details = soup.select('.ul-detailed-view-datasheet li') 
        for i in range(0, 8):
            try:
                details[i] = re.sub('\s+', ' ', details[i].text.replace('\r', '').replace('\n', '').strip())
                ad_info['line ' + str(i + 1)] = details[i].strip()
            except IndexError:
                ad_info['line ' + str(i + 1)] = " "
    else:
        ad_info = ""
    
    return ad_info
            
            
            
            
            
            
            
            
            
            
            
            
            
            