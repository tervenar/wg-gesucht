# import moduli
import numpy as np

# get basic statistics for factor variables
def statistics_factor(data_frame):

    # calculate statistics
    Zweck_WG = data_frame["Zweck-WG"].value_counts()
    Studenten_WG = data_frame["Studenten-WG"].value_counts()
    Frauen_WG = data_frame["Frauen-WG"].value_counts()
    Männer_WG = data_frame["Männer-WG"].value_counts()
    gemischte_WG = data_frame["gemischte WG"].value_counts()
    Vohnheim = data_frame["Wohnheim"].value_counts()
    Verbindung = data_frame["Verbindung"].value_counts()
    Berufstätigen_WG = data_frame["Berufstätigen-WG"].value_counts()
    Business_WG = data_frame["Business-WG"].value_counts()
    Vegetarisch = data_frame["Vegetarisch/Vegan"].value_counts()
    Mehrgenerationen = data_frame["Mehrgenerationen"].value_counts()
    mit_Kindern = data_frame["WG mit Kindern"].value_counts()
    Azubi_WG = data_frame["Azubi-WG"].value_counts()
    Alleinerziehende = data_frame["Alleinerziehende"].value_counts()
    für_Hilfe = data_frame["Wohnen für Hilfe"].value_counts()
    Plus_WG = data_frame["Plus-WG"].value_counts()
    Senioren_WG = data_frame["Senioren-WG"].value_counts()
    inklusive_WG = data_frame["inklusive WG"].value_counts()
    funktionale_WG = data_frame["funktionale WG"].value_counts()
    WG_Neungründung = data_frame["WG-Neungründung"].value_counts()
    Internationals_welcome = data_frame["Internationals welcome"].value_counts()
    LGBTQIA = data_frame["LGBTQIA+"].value_counts()
    renter_gender = data_frame["renter gender"].value_counts()

    return Zweck_WG, Studenten_WG, Frauen_WG, Männer_WG, gemischte_WG, Vohnheim, Verbindung, Berufstätigen_WG, Business_WG, Vegetarisch, Mehrgenerationen, mit_Kindern, Azubi_WG, Alleinerziehende, für_Hilfe, Plus_WG, Senioren_WG, inklusive_WG, funktionale_WG, WG_Neungründung, Internationals_welcome, LGBTQIA, renter_gender
        
def statistics_continuous(data_frame):
    
    # calculate statistics
    
    # hosts age min statistics
    print("Hosts age min statistics")
    print(data_frame["hosts age min"].describe())
    print(np.nanpercentile(data_frame["hosts age min"], [0.5, 1, 2, 3, 4, 5]))
    print("Per cent of unspecified: ", data_frame.isnull().sum()["hosts age min"]/len(data_frame))
    
    # hosts age max statistics
    print("\n")
    print("Hosts age max statistics")
    print(data_frame["hosts age max"].describe())
    print(np.nanpercentile(data_frame["hosts age max"], [95, 96, 97, 98, 99, 100]))
    print("Per cent of unspecified: ", data_frame.isnull().sum()["hosts age max"]/len(data_frame))
    
    # renter age min statistics
    print("\n")
    print("Renter age min statistics")
    print(data_frame["renter age min"].describe())
    print(np.nanpercentile(data_frame["renter age min"], [0.5, 1, 2, 3, 4, 5]))
    print("Per cent of unspecified: ", data_frame.isnull().sum()["renter age min"]/len(data_frame))
    
    # renter age max statistics
    print("\n")
    print("Renter age max statistics")
    print(data_frame["renter age max"].describe())
    print(np.nanpercentile(data_frame["renter age max"], [95, 96, 97, 98, 99, 100]))
    print("Per cent of unspecified: ", data_frame.isnull().sum()["renter age max"]/len(data_frame))
    
