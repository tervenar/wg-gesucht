# import moduli
from .metrics import metrics_gender, metrics_age, metrics_age_hosts, metrics_age_renters, metrics_zweck, metrics_social
import matplotlib.pyplot as plt
import pandas as pd

# plot stacked bar plot with gender preferences of hosts
def plot_gender_preferences(data_frame, city):

    # calculate statistics
    gender_info = metrics_gender(data_frame)
    
    # define plot data
    plotdata = pd.DataFrame({
        "Looking for male only": [gender_info[0], gender_info[3], gender_info[6], gender_info[9], gender_info[12]],
        "Looking for female only": [gender_info[1], gender_info[4], gender_info[7], gender_info[10], gender_info[13]],
        "Looking for mixed": [gender_info[2], gender_info[5], gender_info[8], gender_info[11], gender_info[14]]
        }, 
        index = ["Male only", "Female only", "Male major", "Female major", "Mixed"]
    )
    
    # define plot colors and type
    fig = plotdata[["Looking for mixed", "Looking for male only", "Looking for female only"]].plot(kind = "bar", 
             stacked = True, color = ['#D3D3D3', '#4169E1', '#FFC0CB'], edgecolor = "black").get_figure()
    
    # define axes names and title
    plt.title("Hosts' gender to preferred renters' gender (" + city + ")")
    plt.ylabel("Share for renters' age groups")
    plt.xlabel("Hosts' group")
    plt.show()

    return fig
    
# plot bar plot with age preferences of hosts
def plot_age_preferences(data_frame, city):
    
    # calculate statistics
    age_info = metrics_age(data_frame)
    
    # define plot data
    plotdata = pd.DataFrame({
        "Looking for 18-19": [age_info[0], age_info[1], age_info[2], age_info[3]],
        "Looking for 20-25": [age_info[4], age_info[5], age_info[6], age_info[7]],
        "Looking for 26-30": [age_info[8], age_info[9], age_info[10], age_info[11]],
        "Looking for 31-35": [age_info[12], age_info[13], age_info[14], age_info[15]]
        }, 
        index = ["Hosts 18-20", "Hosts 21-23", "Hosts 24-28", "Hosts 29-35"]
    )

    # define plot colors and type
    fig = plotdata[["Looking for 18-19", "Looking for 20-25", "Looking for 26-30", "Looking for 31-35"]].plot(kind = "bar", colormap = 'YlGnBu', edgecolor = "black").get_figure()

    # define axes names and title
    plt.title("Hosts' age group to preferred renters' age group (" + city + ")")
    plt.ylabel("Share for renters' age groups")
    plt.xlabel("Hosts' age group")
    plt.legend(loc = 'upper right', bbox_to_anchor = (1.08, 1.0))
    plt.show()
    
    return fig

# plot bar plot with zweck preferences of hosts
def plot_zweck_preferences(data_frame, city):
    
    # calculate statistics
    zweck_info = metrics_zweck(data_frame)
    
    # define plot data
    plotdata = pd.DataFrame({
        "Looking for Zweck-WG": [zweck_info[0], zweck_info[3], zweck_info[6], zweck_info[9]],
        "Looking for keine Zweck-WG": [zweck_info[1], zweck_info[4], zweck_info[7], zweck_info[10]],
        "Not defined": [zweck_info[2], zweck_info[5], zweck_info[8], zweck_info[11]]
    }, 
        index = ["Hosts 18-20", "Hosts 21-23", "Hosts 24-28", "Hosts 29-35"]
    )

    # define plot colors and type
    fig = plotdata[["Looking for Zweck-WG", "Looking for keine Zweck-WG", "Not defined"]].plot(kind = "bar", colormap = 'YlOrBr', edgecolor = "black").get_figure()

    # define axes names and title
    plt.title("Hosts' age group to Zweck preferences (" + city + ")")
    plt.ylabel("Share for zweck preference group")
    plt.xlabel("Hosts' age group")
    plt.legend(loc = 'upper right')
    plt.show()
    
    return fig

# plot bar plot with social status preferences of hosts within age groups
def plot_social_preferences(data_frame, city):
    
    # calculate statistics
    social_info = metrics_social(data_frame)
        
    # define plot data
    plotdata = pd.DataFrame({
        "Hosts 18-20": [social_info[0], social_info[1]],
        "Hosts 21-23": [social_info[3], social_info[4]],
        "Hosts 24-28": [social_info[6], social_info[7]],
        "Hosts 29-35": [social_info[9], social_info[10]]
    }, 
        index = ["Studenten-WG", "Berufstätigen-WG"]
    )

    # define plot colors and type
    fig = plotdata[["Hosts 18-20", "Hosts 21-23", "Hosts 24-28", "Hosts 29-35"]].plot(kind = "bar", colormap = 'Set2', edgecolor = "black").get_figure()

    # define axes names and title
    plt.title("Hosts' age group to social status (" + city + ")")
    plt.ylabel("Share for hosts's age group")
    plt.xlabel("Social status")
    plt.legend(loc = 'lower left')
    plt.show()
    
    return fig

# plot bar plot with social status preferences
def plot_social_preferences_2(data_frame, city):
    
    # calculate statistics
    social_info = metrics_social(data_frame)
        
    # define plot data
    plotdata = pd.DataFrame({
        "Hosts": [social_info[12], social_info[13]]
    }, 
        index = ["Studenten-WG", "Berufstätigen-WG"]
    )

    # define plot colors and type
    fig = plotdata[["Hosts"]].plot(kind = "bar", width = 0.15, colormap = 'PuOr', edgecolor = "black", legend = False).get_figure()
        
    # define axes names and title
    plt.title("Social status (" + city + ")")
    plt.ylabel("Share")
    plt.xlabel("Social status")
    plt.legend(loc = 'lower center')
    plt.show()
    
    return fig
    
# plot bar plot with age distribution of hosts
def plot_age_hosts(data_frame, city):

    # caclcilate statistics
    count_age, a, b = metrics_age_hosts(data_frame)
    
    # define graph
    fig, ax = plt.subplots()
    ax.plot(range(a, b), count_age, 'o', color = 'orange')
    fig.suptitle('Share per host\'s age (' + city + ')')
    plt.xlabel('Age of host')
    plt.ylabel('Share value')
    plt.show()
    
    return fig
        
# plot bar plot with age distribution of renters
def plot_age_renters(data_frame, city):

    # caclcilate statistics
    count_age, a, b = metrics_age_renters(data_frame)
    
    # define graph
    fig, ax = plt.subplots()
    ax.plot(range(a, b), count_age, 'o', color = 'orange')
    fig.suptitle('Share per renter\'s age (' + city + ')')
    plt.xlabel('Age of renter')
    plt.ylabel('Share value')
    plt.show()
    
    return fig
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    