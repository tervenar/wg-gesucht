# importing moduli
import pandas as pd
import numpy as np
import math

# define age groups for renters
def segregate_renters(data_frame):
        
    # group of 18-19
    renter_age_cat_1 = []

    # group of 20-25
    renter_age_cat_2 = []

    # group of 26-30
    renter_age_cat_3 = []

    # group of 31-35
    renter_age_cat_4 = []

    for i in range(0, len(data_frame)):
        
        # group of 18-19
        if math.isnan(data_frame.iloc[i]["renter age min"]) or data_frame.iloc[i]["renter age min"] < 20:
            if math.isnan(data_frame.iloc[i]["renter age max"]):
                renter_age_cat_1.append("1")
                renter_age_cat_2.append("1")
                renter_age_cat_3.append("1")
                renter_age_cat_4.append("1")
            elif data_frame.iloc[i]["renter age max"] < 20:
                renter_age_cat_1.append("1")
                renter_age_cat_2.append("0")
                renter_age_cat_3.append("0")
                renter_age_cat_4.append("0")
            elif data_frame.iloc[i]["renter age max"] < 26:
                renter_age_cat_1.append("1")
                renter_age_cat_2.append("1")
                renter_age_cat_3.append("0")
                renter_age_cat_4.append("0")
            elif data_frame.iloc[i]["renter age max"] < 31:
                renter_age_cat_1.append("1")
                renter_age_cat_2.append("1")
                renter_age_cat_3.append("1")
                renter_age_cat_4.append("0")
            elif data_frame.iloc[i]["renter age max"] < 36:
                renter_age_cat_1.append("1")
                renter_age_cat_2.append("1")
                renter_age_cat_3.append("1")
                renter_age_cat_4.append("1")
                
        # group of 20-25
        elif data_frame.iloc[i]["renter age min"] > 19 and data_frame.iloc[i]["renter age min"] < 26:
            if math.isnan(data_frame.iloc[i]["renter age max"]):
                renter_age_cat_1.append("0")
                renter_age_cat_2.append("1")
                renter_age_cat_3.append("1")
                renter_age_cat_4.append("1")
            elif data_frame.iloc[i]["renter age max"] < 26:
                renter_age_cat_1.append("0")
                renter_age_cat_2.append("1")
                renter_age_cat_3.append("0")
                renter_age_cat_4.append("0")
            elif data_frame.iloc[i]["renter age max"] < 31:
                renter_age_cat_1.append("0")
                renter_age_cat_2.append("1")
                renter_age_cat_3.append("1")
                renter_age_cat_4.append("0")
            elif data_frame.iloc[i]["renter age max"] < 36:
                renter_age_cat_1.append("0")
                renter_age_cat_2.append("1")
                renter_age_cat_3.append("1")
                renter_age_cat_4.append("1")
                
        # group of 26-30
        elif data_frame.iloc[i]["renter age min"] > 25 and data_frame.iloc[i]["renter age min"] < 31:
            if math.isnan(data_frame.iloc[i]["renter age max"]):
                renter_age_cat_1.append("0")
                renter_age_cat_2.append("0")
                renter_age_cat_3.append("1")
                renter_age_cat_4.append("1")
            elif data_frame.iloc[i]["renter age max"] < 31:
                renter_age_cat_1.append("0")
                renter_age_cat_2.append("0")
                renter_age_cat_3.append("1")
                renter_age_cat_4.append("0")
            elif data_frame.iloc[i]["renter age max"] < 36:
                renter_age_cat_1.append("0")
                renter_age_cat_2.append("0")
                renter_age_cat_3.append("1")
                renter_age_cat_4.append("1")
        
        # group of 31-35
        elif data_frame.iloc[i]["renter age min"] > 32 and data_frame.iloc[i]["renter age min"] < 36:
            if math.isnan(data_frame.iloc[i]["renter age max"]):
                renter_age_cat_1.append("0")
                renter_age_cat_2.append("0")
                renter_age_cat_3.append("0")
                renter_age_cat_4.append("1")
            elif data_frame.iloc[i]["renter age max"] < 36:
                renter_age_cat_1.append("0")
                renter_age_cat_2.append("0")
                renter_age_cat_3.append("0")
                renter_age_cat_4.append("1")
            
    # adding new columns
    data_frame["renter age 18-19"] = renter_age_cat_1
    data_frame["renter age 20-25"] = renter_age_cat_2
    data_frame["renter age 26-30"] = renter_age_cat_3
    data_frame["renter age 31-35"] = renter_age_cat_4
    
    return data_frame
                
# define age groups for renters
def segregate_hosts(data_frame):
        
    # group of 18-20
    hosts_age_cat_1 = []

    # group of 21-23
    hosts_age_cat_2 = []

    # group of 24-28
    hosts_age_cat_3 = []

    # group of 29-35
    hosts_age_cat_4 = []

    for i in range(0, len(data_frame)):
        
        # group of 18-20
        if math.isnan(data_frame.iloc[i]["hosts age min"]) or data_frame.iloc[i]["hosts age min"] < 21:
            if math.isnan(data_frame.iloc[i]["hosts age max"]):
                hosts_age_cat_1.append("1")
                hosts_age_cat_2.append("1")
                hosts_age_cat_3.append("1")
                hosts_age_cat_4.append("1")
            elif data_frame.iloc[i]["hosts age max"] < 21:
                hosts_age_cat_1.append("1")
                hosts_age_cat_2.append("0")
                hosts_age_cat_3.append("0")
                hosts_age_cat_4.append("0")
            elif data_frame.iloc[i]["hosts age max"] < 24:
                hosts_age_cat_1.append("1")
                hosts_age_cat_2.append("1")
                hosts_age_cat_3.append("0")
                hosts_age_cat_4.append("0")
            elif data_frame.iloc[i]["hosts age max"] < 29:
                hosts_age_cat_1.append("1")
                hosts_age_cat_2.append("1")
                hosts_age_cat_3.append("1")
                hosts_age_cat_4.append("0")
            elif data_frame.iloc[i]["hosts age max"] < 36:
                hosts_age_cat_1.append("1")
                hosts_age_cat_2.append("1")
                hosts_age_cat_3.append("1")
                hosts_age_cat_4.append("1")
                
        # group of 21-23
        elif data_frame.iloc[i]["hosts age min"] > 20 and data_frame.iloc[i]["hosts age min"] < 24:
            if math.isnan(data_frame.iloc[i]["hosts age max"]):
                hosts_age_cat_1.append("0")
                hosts_age_cat_2.append("1")
                hosts_age_cat_3.append("1")
                hosts_age_cat_4.append("1")
            elif data_frame.iloc[i]["hosts age max"] < 24:
                hosts_age_cat_1.append("0")
                hosts_age_cat_2.append("1")
                hosts_age_cat_3.append("0")
                hosts_age_cat_4.append("0")
            elif data_frame.iloc[i]["hosts age max"] < 29:
                hosts_age_cat_1.append("0")
                hosts_age_cat_2.append("1")
                hosts_age_cat_3.append("1")
                hosts_age_cat_4.append("0")
            elif data_frame.iloc[i]["hosts age max"] < 36:
                hosts_age_cat_1.append("0")
                hosts_age_cat_2.append("1")
                hosts_age_cat_3.append("1")
                hosts_age_cat_4.append("1")
                
        # group of 24-28
        elif data_frame.iloc[i]["hosts age min"] > 23 and data_frame.iloc[i]["hosts age min"] < 29:
            if math.isnan(data_frame.iloc[i]["hosts age max"]):
                hosts_age_cat_1.append("0")
                hosts_age_cat_2.append("0")
                hosts_age_cat_3.append("1")
                hosts_age_cat_4.append("1")
            elif data_frame.iloc[i]["hosts age max"] < 29:
                hosts_age_cat_1.append("0")
                hosts_age_cat_2.append("0")
                hosts_age_cat_3.append("1")
                hosts_age_cat_4.append("0")
            elif data_frame.iloc[i]["hosts age max"] < 36:
                hosts_age_cat_1.append("0")
                hosts_age_cat_2.append("0")
                hosts_age_cat_3.append("1")
                hosts_age_cat_4.append("1")
                
        # group of 29-35
        elif data_frame.iloc[i]["hosts age min"] > 28 and data_frame.iloc[i]["hosts age min"] < 36:
            if math.isnan(data_frame.iloc[i]["hosts age max"]):
                hosts_age_cat_1.append("0")
                hosts_age_cat_2.append("0")
                hosts_age_cat_3.append("0")
                hosts_age_cat_4.append("1")
            elif data_frame.iloc[i]["hosts age max"] < 36:
                hosts_age_cat_1.append("0")
                hosts_age_cat_2.append("0")
                hosts_age_cat_3.append("0")
                hosts_age_cat_4.append("1")
    
    # adding new columns
    data_frame["hosts age 18-20"] = hosts_age_cat_1
    data_frame["hosts age 21-23"] = hosts_age_cat_2
    data_frame["hosts age 24-28"] = hosts_age_cat_3
    data_frame["hosts age 29-35"] = hosts_age_cat_4

    return data_frame