from bs4 import BeautifulSoup
import pandas as pd
import requests
import re

# to process several entities for the given city
def process(data_frame):
    
    for i in range(0, len(data_frame)):
        if i == 0:
            curr_frame = process_ad(data_frame.iloc[i]).to_frame().T
        else:
            curr_frame = pd.concat([curr_frame, process_ad(data_frame.iloc[i]).to_frame().T]) # , axis = 0
                
    return curr_frame
    
# to process one page that is an ad itself
def process_ad(ad_info):
    
    # Rent, address, and availability
    ad_info['rent'] = ad_info['rent'].split("€")[0]
    ad_info['deposit'] = ad_info['deposit'].split("€")[0]

    for i in range(0, 8):
        
        # Room info
        if re.search(r'^\d+(m)\w{1}.+(Zimmer).+(in).+\d+(er).+(WG)$', ad_info['line ' + str(i + 1)]):
            ad_info['room info'] = ad_info['line ' + str(i + 1)].strip()
        # Appartment info
        elif re.search(r'^(Wohnungsgr[\u00f6][\u00df]e:)', ad_info['line ' + str(i + 1)]): 
            ad_info['appartment info'] = ad_info['line ' + str(i + 1)].strip()            
        # Hosts' info (gender distribution)
        elif re.search(r'^\d+(er).+(WG).+[(].+\d+.+(Frau|Mann|Frauen|Männer).+[)]$', ad_info['line ' + str(i + 1)]):
            ad_info['host gender'] = ad_info['line ' + str(i + 1)].strip()
        # Hosts' info (age distribution)
        elif re.search(r'^(Bewohneralter:)', ad_info['line ' + str(i + 1)]):
            ad_info['host age'] = ad_info['line ' + str(i + 1)].strip()
        # Smoking allowance
        elif re.search(r'^(Rauchen)', ad_info['line ' + str(i + 1)]):
            ad_info['smoking info'] = ad_info['line ' + str(i + 1)].strip()
        # Sharing info
        elif re.search(r'(-WG|freundlich|welcome|gemischte)', ad_info['line ' + str(i + 1)]):
            ad_info['sharing info'] = ad_info['line ' + str(i + 1)].strip()
        # Language info
        elif re.search(r'^(Sprache/n[:])', ad_info['line ' + str(i + 1)]):
            ad_info['language info'] = ad_info['line ' + str(i + 1)].strip()          
        # Search parameters
        elif re.search(r'^(Geschlecht|Frau|Mann)', ad_info['line ' + str(i + 1)]):
            ad_info['search info'] = ad_info['line ' + str(i + 1)].strip()
    
    # Exceptions
    if 'room info' not in ad_info:
        ad_info['room info'] = " "
    if 'appartment info' not in ad_info:
        ad_info['appartment info'] = " "
    if 'host gender' not in ad_info:
        ad_info['host gender'] = " "
    if 'host age' not in ad_info:
        ad_info['host age'] = " "
    if 'smoking info' not in ad_info:
        ad_info['smoking info'] = " "
    if 'sharing info' not in ad_info:
        ad_info['sharing info'] = " "
    if 'language info' not in ad_info:
        ad_info['language info'] = " "
    if 'search info' not in ad_info:
        ad_info['search info'] = " "
        
    # Room info
    try:
        ad_info['room size'] = ad_info['room info'].split("m")[0].strip()
    except IndexError:
        ad_info['room size'] = " "
    
    try:
        ad_info['number of rooms'] = ad_info['room info'].split("in")[1].split("er")[0].strip()
    except IndexError:
        ad_info['number of rooms'] = " "
    
    # Apartment info
    try:
        ad_info['appartment size'] = ad_info['appartment info'].split(":")[1].strip().split("m")[0]
    except IndexError:
        ad_info['appartment size'] = " "
    
    # Hosts' gender
    if re.search(r'(Frau|Frauen).*(Mann|Männer)', ad_info['host gender']):
        ad_info['female hosts'] = ad_info['host gender'].split("(")[1].split(")")[0].strip().split("und")[0].strip().split(" ")[0]
        ad_info['male hosts'] = ad_info['host gender'].split("(")[1].split(")")[0].strip().split("und")[1].strip().split(" ")[0]
    elif re.search(r'Frau|Frauen', ad_info['host gender']) and not re.search(r'Mann|Männer', ad_info['host gender']):
        ad_info['female hosts'] = ad_info['host gender'].split("(")[1].split(")")[0].strip().split(" ")[0].strip()
        ad_info['male hosts'] = "0"
    elif not re.search(r'Frau|Frauen', ad_info['host gender']) and re.search(r'Mann|Männer', ad_info['host gender']):
        ad_info['male hosts'] = ad_info['host gender'].split("(")[1].split(")")[0].strip().split(" ")[0].strip()
        ad_info['female hosts'] = "0"
    else:
        ad_info['male hosts'] = "0"
        ad_info['female hosts'] = "0"
      
    # Hosts' age   
    if re.search(r'\b(bis)\b', ad_info['host age']):
        ad_info['hosts age min'] = ad_info['host age'].split(":")[1].strip().split("bis")[0].strip()
        ad_info['hosts age max'] = ad_info['host age'].split(":")[1].strip().split("bis")[1].strip().split(" ")[0].strip()
    elif re.search(r'\b(ab)\b', ad_info['host age']):
        ad_info['hosts age min'] = ad_info['host age'].split(":")[1].strip().split("ab")[1].strip().split(" ")[0].strip()
        ad_info['hosts age max'] = " "
    else:
        ad_info['hosts age min'] = " "
        ad_info['hosts age max'] = " "
    
    # Hosts' languages
    if re.search(r'Deutsch', ad_info['language info']):
        ad_info['deutsch'] = "1"
    else:
        ad_info['deutsch'] = "0"

    if re.search(r'Englisch', ad_info['language info']):
        ad_info['englisch'] = "1"
    else:
        ad_info['englisch'] = "0"
    
    
    # Purpose shared and etc. (WRONG LOGIC HERE, LOOK FOR OTHER PARAMETERS)
    
    # Zweck-WG info
    if re.search(r'keine Zweck-WG', ad_info['sharing info']):
        ad_info['Zweck-WG'] = "0"
    elif (re.search(r'Zweck-WG', ad_info['sharing info']) and not re.search(r'keine Zweck-WG', ad_info['sharing info'])):
        ad_info['Zweck-WG'] = "1"
    else:
        ad_info['Zweck-WG'] = "Not defined"
    
    # Studenten-WG info
    if re.search(r'Studenten-WG', ad_info['sharing info']):
        ad_info['Studenten-WG'] = "1"
    else:
        ad_info['Studenten-WG'] = "0"    
    
    # Frauen-WG info
    if re.search(r'Frauen-WG', ad_info['sharing info']):
        ad_info['Frauen-WG'] = "1"
    else:
        ad_info['Frauen-WG'] = "0" 
    
    # Männer-WG info
    if re.search(r'Männer-WG', ad_info['sharing info']):
        ad_info['Männer-WG'] = "1"
    else:
        ad_info['Männer-WG'] = "0"
    
    # gemischte WG info
    if re.search(r'gemischte WG', ad_info['sharing info']):
        ad_info['gemischte WG'] = "1"
    else:
        ad_info['gemischte WG'] = "0"
        
    # Wohnheim info
    if re.search(r'Wohnheim', ad_info['sharing info']):
        ad_info['Wohnheim'] = "1"
    else:
        ad_info['Wohnheim'] = "0"
    
    # Verbindung info
    if re.search(r'Verbindung', ad_info['sharing info']):
        ad_info['Verbindung'] = "1"
    else:
        ad_info['Verbindung'] = "0"
    
    # Berufstätigen-WG info
    if re.search(r'Berufstätigen-WG', ad_info['sharing info']):
        ad_info['Berufstätigen-WG'] = "1"
    else:
        ad_info['Berufstätigen-WG'] = "0"
    
    # Business-WG info
    if re.search(r'Business-WG', ad_info['sharing info']):
        ad_info['Business-WG'] = "1"
    else:
        ad_info['Business-WG'] = "0"
    
    # Vegetarisch/Vegan info
    if re.search(r'Vegetarisch/Vegan', ad_info['sharing info']):
        ad_info['Vegetarisch/Vegan'] = "1"
    else:
        ad_info['Vegetarisch/Vegan'] = "0"
        
    # Mehrgenerationen info
    if re.search(r'Mehrgenerationen', ad_info['sharing info']):
        ad_info['Mehrgenerationen'] = "1"
    else:
        ad_info['Mehrgenerationen'] = "0"
        
    # WG mit Kindern info
    if re.search(r'WG mit Kindern', ad_info['sharing info']):
        ad_info['WG mit Kindern'] = "1"
    else:
        ad_info['WG mit Kindern'] = "0"
        
    # Azubi-WG info
    if re.search(r'Azubi-WG', ad_info['sharing info']):
        ad_info['Azubi-WG'] = "1"
    else:
        ad_info['Azubi-WG'] = "0"
    
    # Alleinerziehende info
    if re.search(r'Alleinerziehende', ad_info['sharing info']):
        ad_info['Alleinerziehende'] = "1"
    else:
        ad_info['Alleinerziehende'] = "0"
    
    # Wohnen für Hilfe info
    if re.search(r'Wohnen für Hilfe', ad_info['sharing info']):
        ad_info['Wohnen für Hilfe'] = "1"
    else:
        ad_info['Wohnen für Hilfe'] = "0"
        
    # Plus-WG info
    if re.search(r'Plus-WG', ad_info['sharing info']):
        ad_info['Plus-WG'] = "1"
    else:
        ad_info['Plus-WG'] = "0"
        
    # Senioren-WG info
    if re.search(r'Senioren-WG', ad_info['sharing info']):
        ad_info['Senioren-WG'] = "1"
    else:
        ad_info['Senioren-WG'] = "0"
        
    # inklusive WG info
    if re.search(r'inklusive WG', ad_info['sharing info']):
        ad_info['inklusive WG'] = "1"
    else:
        ad_info['inklusive WG'] = "0"
    
    # funktionale WG info
    if re.search(r'funktionale WG', ad_info['sharing info']):
        ad_info['funktionale WG'] = "1"
    else:
        ad_info['funktionale WG'] = "0"
    
    # WG-Neungründung info
    if re.search(r'WG-Neungründung', ad_info['sharing info']):
        ad_info['WG-Neungründung'] = "1"
    else:
        ad_info['WG-Neungründung'] = "0"
        
    # Internationals welcome info
    if re.search(r'Internationals welcome', ad_info['sharing info']):
        ad_info['Internationals welcome'] = "1"
    else:
        ad_info['Internationals welcome'] = "0"
        
    # LGBTQIA+ info
    if re.search(r'LGBTQIA+', ad_info['sharing info']):
        ad_info['LGBTQIA+'] = "1"
    else:
        ad_info['LGBTQIA+'] = "0"
    
    # Gender search parameters
    if re.search(r'(Frau|Frauen)', ad_info['search info']):
        ad_info['renter gender'] = "Female"
    elif re.search(r'(Mann|Männer)', ad_info['search info']):
        ad_info['renter gender'] = "Male"
    elif re.search(r'Divers', ad_info['search info']):
        ad_info['renter gender'] = "Diverse"
    elif bool(re.search(r'Geschlecht egal', ad_info['search info'])):
        ad_info['renter gender'] = "Not important"
    else:
        ad_info['renter gender'] = " "
    
#     # Age search parameters
    if re.search("zwischen", ad_info['search info']):
        ad_info['renter age min'] = ad_info['search info'].split("zwischen")[1].strip().split("und")[0].strip()
        ad_info['renter age max'] = ad_info['search info'].split("zwischen")[1].strip().split("und")[1].strip().split(" ")[0].strip()
    elif re.search("ab", ad_info['search info']):
        ad_info['renter age min'] = ad_info['search info'].split("ab")[1].strip().split(" ")[0].strip()
        ad_info['renter age max'] = " "
    elif re.search("bis", ad_info['search info']):
        ad_info['renter age min'] = " "
        ad_info['renter age max'] = ad_info['search info'].split("bis")[1].strip().split(" ")[0].strip()
    else:
        ad_info['renter age min'] = " "
        ad_info['renter age max'] = " "
    
    [ad_info.pop("line " + str(i + 1)) for i in range(0, 8)];
        
    return ad_info













