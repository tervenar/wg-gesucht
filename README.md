# Computational Social Science Lab (group WG-Gesucht 1)

In the following project we study certain parameters that might affect the process of renting a shared apartment. This study will focus on answering the proposed questions on the social aspects of accommodation services in five cities in Bayern.

## Description

1. Folder 'data' contains .csv files used throughout the project:
- 'raw_data' - unprocessed files in a separate folder for each city,
- 'processed_data' - merged files with all the necessary features.

2. Folder 'scripts' is an authentic package with all the algorithms and complementary code used:
- 'scraper' - contains moduli to scrape ads from wg-gesucht.de,
- 'processing' - contains moduli to derive features from raw data,
- 'statistics' - contains moduli to analyse the features,
- 'segregation' - contains moduli to create age group categories,
- 'metrics' - contains moduli to calculate the metrics used to answer the hypotheses,
- 'graphs' - contains moduli to visualise the results.

3. Folder 'main' contains .ipynb files that present the main results:
- 'scraping' - report for scraping execution,
- 'features' - report for factor and continuous feature analysis,
- 'hypotheses' - report for the analysis of proposed answers and implemented metrics.

4. Folder 'figures' contains .jpeg files that are the results of the analysis from folder 'main' in a separate folder for each city.

## Getting Started

### Dependencies

Prerequisites needed before running the program:
1. for scraping: pandas, BeautifulSoup
2. for visualisation and analysis: matplotlib

### Installing

All the folders except for 'figures' are subject for download to run the code.

### Executing program

1. for scraping: run chunks in '*_wg.ipynb' in 'main/scraping' folder,
2. for analysis of features: run chunks in '*_statistics.ipynb' in 'main/features' folder,
3. for analysis of hypotheses: run chunks in '*_metrics.ipynb' in 'main/hypotheses' folder.

## Help

If errors while scraping occur, increase the waiting time between each add.

## Authors

Contributors names and contact info

* Pavel Prudnikov

## Version History

* 0.1
    * Initial scraping
    * Data processing
* 0.2
    * Feature analysis
* 0.3
    * Introduction of metrics and analysis of hypotheses

## License

This project is licensed under the open-source license.

## Acknowledgments

Inspiration, code snippets, etc.
* [StackExchange](https://stackexchange.com)
